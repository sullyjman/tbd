
## TBD

### Event details (sent via Teams)

#### Teams Channel: 
!RSD-Intermediate Training (training is only offered
via Teams – no in-person offering)

#### Training Times: 
Sunday with core training hours from 9-11 & 12-1500
(these times are available on most Sq schedules). However, trainers
will be available as early as 7 so members may start early, schedule
permitting, and trainers will be available until 1600.

#### Overview: 
Maj Fryer and MSgt Patrick from 276 COS are kind enough to
host our Oct RSD training. Here’s what members can expect: “Tunneling
and Unix enumeration with hands on labs focusing on understanding
environmental clues to solve spooky puzzles and practicing key
redirection spells that every ghoul should have in their grimoire. Too
spooky for some, Intermediate level training (IST completion
recommended).” Labs will be Instructor-led and walk-throughs of
solutions will also be provided throughout the day and recorded.

#### Logistics: 
A computer with internet access and Teams. Members will
ideally have a PCTE account but efforts will be made to pair
individuals based upon experience and to ensure one person has a PCTE
account. It's recommended member's test their PCTE account prior to
RSD to ensure they can login. A video camera with mic is helpful for
collaboration but not required.
REQUIRED FOR THOSE WHO PLAN TO ATTEND! Members must complete this
simple 5 question sign-up form NLT 1500 on Sat so our trainers can be
prepared to effectively train on Sun. 

[Form Link](https://forms.microsoft.com/Pages/ResponsePage.aspx?id=s_usIb4yFUeQJR4vAVy76UEnk2chioJPhEwjXFMOazdUOFYzSExKU1VYVlcxVzdXTFNRMTdBMzdDWC4u)

## Supporting software
sudo apt install docker.io docker-compose

