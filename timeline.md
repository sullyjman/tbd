
## Timeline

#### 0700 arrive in channel

  Get people:
  - Into CTFd
  - Assigned a bchd
  - Aware of schedule

#### 0900 Kickoff video calls

  - Talk about schedule
  - Talk about access to bchds and assignments
  - Talk about shared hosting and being nice to the VPS
  - Brief slides

#### 1100 First Demo Session

#### 1200 Demo Session
 
#### 1300 Demo Session

#### 1400 Demo Session
 
#### 1500 Last  Demo Session
