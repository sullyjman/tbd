## Networks/Hosts

### New Networks
| Name                      | Subnet/Mask      | Spooky Level |
|---------------------------|------------------|--------------|
| Trick-or-Treat (Internet) | 200.200.200.0/24 | Spoopy       |
| Halloween Party           | 172.16.2.0/24    | P Spoopy     |
| Back Yard Cemetary        | 10.10.10.0/24    | Spooky       |
| Crypt                     | 192.168.200.0/24 | 2Spooky      |

### New hosts
| Name                     | First Interface | Second Interface |
|--------------------------|-----------------|------------------|
| beachheads&redirectors   | 200.200.200.X   |                  |
| yachtzee-router-firewall | 172.16.2.1      | 200.200.200.1    |
| yachtzee-server-1        | 172.16.2.100    | 10.10.10.44      | 
| yachtzee-server-2        | 10.10.10.55     | 192.168.200.66   |
| yachtzee-server-3        | 192.168.200.89  |                  |

## Scenario
beachhead:
 - gotty, 200.200.200.X, assigned per user/team
 - throwaway BOO flag in ~/flag.txt 
   {BOO:59797335-6254-40bd-813b-e3f655f702c3}
 - some file based puzzle to get IP of the party's (must be through redirectors)

redirectors:
 - port scan required to find the service, known password
 - ~/flag.txt
   {BOO:91596f7d-88f1-454f-82ba-3d61c48b2e73}
 - /tmp/flag.txt
   {BOO:668084b2-c340-46c3-b8d4-945db6e7cc0b}

router-firewall:
 - no shell, only port forward via key
 - /bin/nope shell
   {BOO:4636fdbb-adc1-48f1-8b3e-d8657dea2ba0}
 - proxychains nmap syn scan for next 

server-1:
 - MOTD
   {BOO:dc22e9db-9225-4082-b7ed-78f08784fd54}
 - ~/evidence_found_lense.enc
   {BOO:8e75a07e-48ad-4b5a-b5a9-9795141f3942}
 - gateway ports = true
   comment on /etc/ssh/sshd_config
   {BOO:e9d5e38d-8179-4e06-93ca-d5cce06c2099}
 - getting periodically connected to on port 1031
 - port foward back to nc to get details of next and flag
   {BOO:96239742-a253-40e8-bbb0-fba44b230cee}

server-2:
 - access: found username and key
 - MOTD
   {BOO:72d0e731-b6ef-4e43-843a-5dc57068ae6e}
 - note-to-self.txt
   contains public key of user and flag
 - getting connected to by a ssh client
   upon successful connection runs printf command user+key+flag
   {BOO:20b2523a-1f11-46e8-beed-d40bba98aab0}

server-3:
 - access: found username and key
 - Final flag
   flag.txt
   {BOO:88c905ce-f09c-4d34-a2ea-25976016e91f}



#### flag generateor
>>> import uuid
>>> for _ in range(1,49): print("{"+f"BOO:{str(uuid.uuid4())}"+"}")
